#!/usr/bin/env bash
# Update a DRP Endpoint and all Catalog content/plugin items to specified VERSION

set -e

###
#  TODO:
#        * check current installed version against requested version
#          (if possible) and then update only if a newer version requested
#        * add option to skip all content or all plugins
#        * add a verification stage, after output of ITEMS and SKIP, optional
#          argument to override the verification y/n question
#        * this should be folded in to 'install.sh' or 'drpcli catalo' usage
###

###
#  allow overriding our catalog file and which 'drpcli' binary to use
#  VERSION set for upgrade versions
#  FILTERS are space separated content packs to NOT upgrade
###
CATALOG=${CATALOG:-"$(mktemp catalog-tmp-XXXXXXXX.json)"}
DRPCLI=${DRPCLI}
VERSION=${VERSION:-"stable"}
FILTERS=${FILTERS:-""}

usage() {

cat <<EO_USAGE
USAGE: $0 [ -u ] [ -c catalog_json_file ] [-v version] [ -f "filt_1,filt_2" ] [ -d drpcli_binary_path ]

WHERE:
       -u                      this Usage statement
       -c catalog_json_file    an existing Catalog JSON file to use
                               (default online download of new catalog)
       -v version              version of DRP and Content/Plugins to update to
                               (default "stable")
       -f "filt_1,filt_2"      comma, colon, or space separated (quote protection
                               required) list of contents to NOT update (filter out)
                               (no filters set by default)
       -d drpcli_binary_path   an alternate "drpcli" binary to use from the one
                               found by default in the PATH

VARIABLES:
      Shell variables can be used instead of command line flags.  Command line
      flags will override Shell Variable contents if they are specified.  Variables
      that are available to use:

      CATALOG                  catalog_json_file
      DRPCLI                   drpcli_binary_path
      VERSION                  version
      FILTERS                  "filt_1 filt_2"
  or  FILTERS                  filt_1,filt_2

EXAMPLES:

       # update DRP and all content to "tip" version:
       $0 -v tip

       # update DRP and all content EXCEPT "task-library" and "dev-library" to "tip:
       $0 -v tip -f "task-library dev-library"

       # set exclude FILTERS via Variable, and upgrade everything to default current
       # "stable" version in the online catalog
       FILTERS="task-library,dev-library" $0
EO_USAGE
}

xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; printf "FATAL: $*\n"; exit $XIT; }

###
#  If $CATALOG file does not exist, then get it online, otherwise, use
#  the existing found catalog
###
get_catalog() {
  if [[ "$CATALOG" =~ ^catalog-tmp-.*json ]]
  then
    echo "Getting local copy of Catalog JSON to file '$CATALOG'"
    $DRPCLI catalog show > $CATALOG
  else
    echo "Reusing existing catalog found at '$CATALOG'"
  fi
}

###
#  Given catalog item name for $1, and Version for $2, return
#  true or false if it exists in the catalog
###
in_catalog() {
  local _item="$1"
  local _ver="$2"

  # FOUND_VERSIONS can be used for error output later on
  FOUND_VERSIONS=( $($DRPCLI -c $CATALOG catalog item show "$_item" 2> /dev/null | $JQ -r '.Versions[]') )

  [[ " ${FOUND_VERSIONS[@]} " =~ " ${_ver} " ]] && echo "true" || echo "false"
}

###
#  skip processing if not found in the catalog with given version
###
process_skip() {
  SKIP_LIST+=( "$(printf "%-25s %s" $ITEM "(no version in catalog)")" )
  echo "Item '$ITEM' with version '$VERSION' not found in Catalog, skipping."
}

###
#  Process the install for DRP Endpoint
###
update_drp() {
  if [[ $(in_catalog drp $VERSION) == "true" ]]
  then
    echo ">>> Installing 'drp' with version '$VERSION'"
    echo "CMD: $DRPCLI catalog item install drp --version=$VERSION"
    $DRPCLI catalog item install drp --version=$VERSION
  else
    echo "Requested version '$VERSION' not found in catalog. Catalog has:"
    echo "${FOUND_VERSIONS[*]}"
    xiterr 1 "unable to upgrade DRP, stop processing"
  fi
}

###
#  Wait until DRP is ready to service API calls
###
check_drp_ready() {
  COUNT=0
  while ! $DRPCLI info get > /dev/null 2>&1 ; do
    if (( $COUNT == 0 )); then
      echo -n "Waiting for dr-provision to start ..."
    else
      echo -n "."
    fi
    sleep 2
    # Pre-increment for compatibility with Bash 4.1+
    ((++COUNT))
    if (( $COUNT > 10 )) ; then
      echo
      xiterr 1 "FATAL: DRP Failed to start"
    fi
  done
  echo
}

###
#  Process any command line options, and overide the appropriate variables with them
###
while getopts ":v:c:f:d:u" CmdLineOpts
do
  case $CmdLineOpts in
    v) VERSION=${OPTARG} ;;
    c) CATALOG=${OPTARG} ;;
    d) DRPCLI=${OPTARG}  ;;
    u) usage; exit 0     ;;
    f) FILTERS=$(echo ${OPTARG} | sed 's/[,:]/ /g') ;;
    *) echo "Unknown argument '$OPTARG'."
       usage
       exit 1
       ;;
  esac
done

###
#  make sure our core required drpcli and jq tools are available
###
if [[ -n "$DRPCLI" ]]
then
  $DRPCLI version 2&>1 /dev/null || xiterr 1 "'drpcli' binary at '$DRPCLI' failed to operate correctly"
else
  DRPCLI=$(which drpcli)
fi

if which jq 2&>1 /dev/null
then
  JQ=$(which jq)
else
  if which drpjq 2&>1 /dev/null
  then
    JQ=$(which drpjq)
  else
    cp $(which drpcli) myjq
    chmod 755 myjq
    JQ="$(pwd)/myjq"
  fi
fi

###
#  Verify we are talking to a running DRP Endpoint
###
echo "Testing DRP Endpoint service is running ... "
$DRPCLI info get > /dev/null

###
#  Get our catalog locally (or use specified local existing file), then
#  process the DRP Endpoint upgrade, and wait until service is ready
###
get_catalog
update_drp
check_drp_ready

###
#  Set our content/plugin items to be installed, removing our FILTERS
#  items (plus "rackn-license")
###
ITEMS=( $($DRPCLI contents list | $JQ -r '.[].meta | select(.Type=="dynamic") | .Name') )
ITEMS+=( $($DRPCLI plugin_providers list | $JQ -r '.[].Name' ) )

for DEL in rackn-license $FILTERS
do
  SKIP+="$DEL "
  SKIP_LIST+=( "$(printf "%-25s %s" $DEL "(filtered by request)")" )
  ITEMS=( "${ITEMS[@]/$DEL}" )
done

echo ""
echo ">>> INSTALL requested for:"
echo "    ${ITEMS[*]}"
echo ">>> SKIPPING by filter request:"
echo "    $SKIP"
echo ""

###
#  Process our install items that haven't been filtered
###

for ITEM in ${ITEMS[*]}
do
  if [[ $(in_catalog $ITEM $VERSION) == "true" ]]
  then
    echo ">>> Installing '$ITEM' with version '$VERSION'"
    $DRPCLI catalog item install $ITEM --version=$VERSION > /dev/null
    INST+="$ITEM "
  else
    echo "WARNING:  for '$ITEM' no version '$VERSION' found in catalog, valid versions:"
    echo "${FOUND_VERSIONS[*]}"
    process_skip
  fi
done

if [[ "$CATALOG" =~ ^catalog-tmp-.*json ]]
then
  rm -f $CATALOG
else
  echo "not removing catalog file"
fi

[[ -r myjq ]] && rm -f myjq

echo ">>> SUMMARY"
echo ">>> INSTALLED:  $INST"
echo ">>> SKIPPED:  "
for SKIP in "${SKIP_LIST[@]}"; do echo "             $SKIP"; done
echo ""
