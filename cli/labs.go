package cli

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os/exec"
	"path"
	"strings"
	"text/template"

	"gitlab.com/rackn/provision/v4/models"

	"github.com/spf13/cobra"
)

func init() {
	addRegistrar(labs)
}

func labs(app *cobra.Command) {
	tree := addLabCommands()
	app.AddCommand(tree)
}

func getLab(input string) (*models.Lab, error) {
	var lab *models.Lab
	if err := bufOrFileDecode(input, &lab); err != nil {
		return nil, err
	}
	lab.Fill()
	lab.Validate()
	if err := lab.HasError(); err != nil {
		return nil, err
	}
	return lab, nil
}

func addLabCommands() (res *cobra.Command) {
	name := "labs"
	res = &cobra.Command{
		Use:   name,
		Short: fmt.Sprintf("Access CLI commands relating to %v", name),
	}

	res.AddCommand(&cobra.Command{
		Use:   "validate [-|json string|file]",
		Short: "Validate the lab for correctness",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("showHa requires one argument")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			lab, err := getLab(args[0])
			if err != nil {
				return err
			}
			return prettyPrint(lab)
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "document [template file] [-|json string|file]",
		Short: "Generate Markdown documentation for the lab.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("requires two arguments")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			lab, err := getLab(args[1])
			if err != nil {
				return err
			}

			tmplFmt, err := ioutil.ReadFile(args[0])
			if err != nil {
				return fmt.Errorf("Failed to open template %s: %v", args[0], err)
			}

			tmpl := template.New("installLines").Funcs(models.DrpSafeFuncMap()).Option("missingkey=error")
			tmpl, err = tmpl.Parse(string(tmplFmt))
			if err != nil {
				return err
			}

			ldd := &LabDocData{
				Lab:        lab,
				LabSection: nil,
				Depth:      0,
			}

			if lab.VideoUrl != "" {
				url, err := url.Parse(lab.VideoUrl)
				if err != nil {
					log.Panicf("Failed to parse VideoUrl: %v", err)
				}
				ldd.VideoTag = url.Query().Get("v")
				if ldd.VideoTag == "" {
					ldd.VideoTag = path.Base(url.Path)
				}
				ldd.VideoTime = url.Query().Get("t")
				if ldd.VideoTag == "" {
					ldd.VideoTime = url.Query().Get("start")
				}
			}

			buf2 := &bytes.Buffer{}
			err = tmpl.Execute(buf2, ldd)
			if err == nil {
				fmt.Println(string(buf2.Bytes()))
			}
			return err
		},
	})
	return res
}

type LabDocData struct {
	Lab        *models.Lab
	LabSection *models.LabSection
	Depth      int
	InTab      bool
	VideoTag   string
	VideoTime  string
}

func (dd *LabDocData) ToRef(names ...string) string {
	outputs := []string{}
	for _, s := range names {
		outputs = append(outputs, strings.ReplaceAll(s, " ", "-"))
	}
	return strings.Join(outputs, "_")
}

func (dd *LabDocData) MarkdownToRst(data string) string {
	if err := ioutil.WriteFile("tmpfile.input", []byte(data), 0644); err != nil {
		log.Panicf("Write: %v", err)
	}

	app := "pandoc"
	arg0 := "--from=markdown"
	arg1 := "--to=rst"
	arg2 := "--output=tmpfile.output"
	arg3 := "tmpfile.input"
	cmd := exec.Command(app, arg0, arg1, arg2, arg3)
	_, err := cmd.Output()
	if err != nil {
		log.Panicf("%v", err)
	}

	output, err := ioutil.ReadFile("tmpfile.output")
	if err != nil {
		log.Panicf("Read: %v", err)
	}

	answer := strings.ReplaceAll(string(output), "ux://", "https://portal.rackn.io/#/e/0.0.0.0/")
	return answer
}

func (dd *LabDocData) LengthString(str, pat string) string {
	l := len(str)
	out := ""
	for i := 0; i < l; i++ {
		out += pat
	}
	return out
}

func (dd *LabDocData) Encapsulate(str, first, pat string) string {
	tmp := strings.ReplaceAll(str, "\n", "\n"+pat)
	return first + tmp
}

func (dd *LabDocData) RenderSection(tmplFile string, ls *models.LabSection, depth int, tab string) string {
	tmplFmt, err := ioutil.ReadFile(tmplFile)
	if err != nil {
		log.Panicf("Failed to open template %s: %v", tmplFile, err)
	}

	tmpl := template.New("installLines").Funcs(models.DrpSafeFuncMap()).Option("missingkey=error")
	tmpl, err = tmpl.Parse(string(tmplFmt))
	if err != nil {
		log.Panicf("%v", err)
	}

	ldd := &LabDocData{
		Lab:        dd.Lab,
		LabSection: ls,
		InTab:      tab != "",
		Depth:      depth + 1,
	}

	buf2 := &bytes.Buffer{}
	err = tmpl.Execute(buf2, ldd)
	if err == nil {
		return string(buf2.Bytes())
	}
	log.Panicf("%v", err)
	return ""
}
