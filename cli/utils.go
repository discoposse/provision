package cli

import (
	"bytes"
	"crypto/sha256"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/Masterminds/semver"
	"github.com/VictorLowther/jsonpatch2/utils"
	"github.com/mattn/go-isatty"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"golang.org/x/sync/errgroup"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"sort"
	"strconv"
	"strings"
)

var encodeJSONPtr = strings.NewReplacer("~", "~0", "/", "~1")

// String translates a pointerSegment into a regular string, encoding it as we go.
func makeJSONPtr(s string) string {
	return encodeJSONPtr.Replace(string(s))
}

var defaultDrpVersion = "stable"
var defaultUxVersion = ""

func bufOrFileDecode(ref string, data interface{}) (err error) {
	buf, terr := bufOrStdin(ref)
	if terr != nil {
		err = fmt.Errorf("Unable to process reference object: %v", terr)
		return
	}
	err = api.DecodeYaml(buf, &data)
	if err != nil {
		err = fmt.Errorf("Unable to unmarshal reference object: %v", err)
		return
	}
	return
}

func getCatalogSource(nv string) (string, error) {
	// XXX: Query self first?  One day.
	clayer, err := fetchCatalog()
	if err != nil {
		return "", err
	}
	var elem interface{}
	for k, cobj := range clayer.Sections["catalog_items"] {
		if k == nv {
			elem = cobj
			break
		}
	}
	if elem == nil {
		return "", fmt.Errorf("Catalog item: %s not found", nv)
	}

	ci := &models.CatalogItem{}
	if err := utils.Remarshal(elem, &ci); err != nil {
		return "", fmt.Errorf("Catalog item: %s can not be remarshaled: %v", nv, err)
	}
	if ci.Source == "" {
		return "", fmt.Errorf("Catalog item: %s does not have a source: %v", nv, ci)
	}
	return ci.DownloadUrl(runtime.GOOS, runtime.GOARCH), nil
}

func urlOrFileAsReadCloser(src string) (io.ReadCloser, error) {
	if s, err := os.Lstat(src); err == nil && s.Mode().IsRegular() {
		fi, err := os.Open(src)
		if err != nil {
			return nil, fmt.Errorf("Error opening %s: %v", src, err)
		}
		return fi, nil
	}
	if strings.HasPrefix(src, "catalog:") {
		var err error
		src, err = getCatalogSource(strings.TrimPrefix(src, "catalog:"))
		if err != nil {
			return nil, err
		}
	}

	if u, err := url.Parse(src); err == nil && (u.Scheme == "http" || u.Scheme == "https") {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		if downloadProxy != "" {
			proxyURL, err := url.Parse(downloadProxy)
			if err == nil {
				tr.Proxy = http.ProxyURL(proxyURL)
			}
		}
		if Session != nil {
			src, _ = Session.SignRackNUrl(src)
		}
		client := &http.Client{Transport: tr}
		res, err := client.Get(src)
		if err != nil {
			return nil, err
		}
		return res.Body, nil
	} else if err == nil && u.Scheme == "file" {
		if err != nil {
			return nil, err
		}
		// get the host and path and just open the file
		fi, err := os.Open(filepath.Join(u.Host, u.Path))
		if err != nil {
			return nil, fmt.Errorf("error opening %s: %v", filepath.Join(u.Host, u.Path), err)
		}
		return fi, nil
	}
	return nil, fmt.Errorf("Must specify a file or url")
}

func bufOrFile(src string) ([]byte, error) {
	if s, err := os.Lstat(src); err == nil && s.Mode().IsRegular() {
		return ioutil.ReadFile(src)
	}

	if strings.HasPrefix(src, "catalog:") {
		var err error
		src, err = getCatalogSource(strings.TrimPrefix(src, "catalog:"))
		if err != nil {
			return nil, err
		}
	}

	if u, err := url.Parse(src); err == nil && (u.Scheme == "http" || u.Scheme == "https") {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		if downloadProxy != "" {
			proxyURL, err := url.Parse(downloadProxy)
			if err == nil {
				tr.Proxy = http.ProxyURL(proxyURL)
			}
		}
		if Session != nil {
			src, _ = Session.SignRackNUrl(src)
		}
		client := &http.Client{Transport: tr}
		res, err := client.Get(src)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		return []byte(body), err
	} else if err == nil && u.Scheme == "file" {
		src = filepath.Join(u.Host, u.Path)
		if s, err := os.Lstat(src); err == nil && s.Mode().IsRegular() {
			return ioutil.ReadFile(src)
		}
	}
	return []byte(src), nil
}

func bufOrStdin(src string) ([]byte, error) {
	if src == "-" {
		return ioutil.ReadAll(os.Stdin)
	}
	return bufOrFile(src)
}

func into(src string, res interface{}) error {
	buf, err := bufOrStdin(src)
	if err != nil {
		return fmt.Errorf("Error reading from stdin: %v", err)
	}
	return api.DecodeYaml(buf, &res)
}

func mergeInto(src models.Model, changes []byte) (models.Model, error) {
	dest := models.Clone(src)
	dec := json.NewDecoder(bytes.NewReader(changes))
	dec.DisallowUnknownFields()
	err := dec.Decode(&dest)
	return dest, err
}

func mergeFromArgs(src models.Model, changes string) (models.Model, error) {
	// We have to load this and then convert to json to merge safely.
	data := map[string]interface{}{}
	if err := bufOrFileDecode(changes, &data); err != nil {
		return nil, err
	}
	buf, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	return mergeInto(src, buf)
}

func d(msg string, args ...interface{}) {
	if debug {
		log.Printf(msg, args...)
	}
}

func truncateString(str string, num int) string {
	bnoden := str
	if len(str) > num {
		if num > 3 {
			num -= 3
		}
		bnoden = str[0:num] + "..."
	}
	return bnoden
}

func lamePrinter(obj interface{}) []byte {
	isTable := format == "table"

	if slice, ok := obj.([]interface{}); ok {
		tableString := &strings.Builder{}
		table := tablewriter.NewWriter(tableString)

		var theFields []string
		data := [][]string{}

		colColors := []tablewriter.Colors{}
		headerColors := []tablewriter.Colors{}
		for i, v := range slice {
			if m, ok := v.(map[string]interface{}); ok {
				if i == 0 {
					theFields = strings.Split(printFields, ",")
					if printFields == "" {
						theFields = []string{}
						for k := range m {
							theFields = append(theFields, k)
						}
					}
					if !noColor {
						for range theFields {
							headerColors = append(headerColors, tablewriter.Color(colorPatterns[4]...))
							colColors = append(colColors, tablewriter.Color(colorPatterns[6]...))
						}
					}
				}
				row := []string{}
				for _, k := range theFields {
					row = append(row, truncateString(fmt.Sprintf("%v", m[k]), truncateLength))
				}
				data = append(data, row)
			} else {
				if i == 0 {
					theFields = []string{"Index", "Value"}
					if !noColor {
						headerColors = []tablewriter.Colors{tablewriter.Color(colorPatterns[4]...), tablewriter.Color(colorPatterns[5]...)}
						colColors = []tablewriter.Colors{tablewriter.Color(colorPatterns[6]...), tablewriter.Color(colorPatterns[7]...)}
					}
				}
				data = append(data, []string{fmt.Sprintf("%d", i), truncateString(fmt.Sprintf("%v", obj), truncateLength)})
			}
		}

		if !noHeader {
			table.SetHeader(theFields)
			table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
			table.SetHeaderLine(isTable)
			if !noColor {
				table.SetHeaderColor(headerColors...)
				table.SetColumnColor(colColors...)
			}
		}
		table.SetAutoWrapText(false)
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		if !isTable {
			table.SetCenterSeparator("")
			table.SetColumnSeparator("")
			table.SetRowSeparator("")
			table.SetTablePadding("\t") // pad with tabs
			table.SetBorder(false)
			table.SetNoWhiteSpace(true)
		}
		table.AppendBulk(data) // Add Bulk Data
		table.Render()
		return []byte(tableString.String())
	}
	if m, ok := obj.(map[string]interface{}); ok {
		theFields := strings.Split(printFields, ",")
		tableString := &strings.Builder{}
		table := tablewriter.NewWriter(tableString)

		if !noHeader {
			table.SetHeader([]string{"Field", "Value"})
			table.SetHeaderLine(isTable)
			if !noColor {
				table.SetHeaderColor(tablewriter.Color(colorPatterns[4]...), tablewriter.Color(colorPatterns[5]...))
				table.SetColumnColor(tablewriter.Color(colorPatterns[6]...), tablewriter.Color(colorPatterns[7]...))
			}
			table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
		}
		table.SetAutoWrapText(false)
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		if !isTable {
			table.SetCenterSeparator("")
			table.SetColumnSeparator("")
			table.SetRowSeparator("")
			table.SetTablePadding("\t") // pad with tabs
			table.SetBorder(false)
			table.SetNoWhiteSpace(true)
		}

		data := [][]string{}

		if printFields != "" {
			for _, k := range theFields {
				data = append(data, []string{k, truncateString(fmt.Sprintf("%v", m[k]), truncateLength)})
			}
		} else {
			index := []string{}
			for k := range m {
				index = append(index, k)
			}
			sort.Strings(index)
			for _, k := range index {
				v := m[k]
				data = append(data, []string{k, truncateString(fmt.Sprintf("%v", v), truncateLength)})
			}
		}

		table.AppendBulk(data) // Add Bulk Data
		table.Render()
		return []byte(tableString.String())
	}

	// Default for everything else
	return []byte(truncateString(fmt.Sprintf("%v", obj), truncateLength))
}

var colorPatterns [][]int

func processColorPatterns() {
	if colorPatterns != nil {
		return
	}

	colorPatterns = [][]int{
		// JSON
		{32},    // String
		{33},    // Bool
		{36},    // Number
		{90},    // Null
		{34, 1}, // Key
		// Table colors
		{35}, // Header
		{92}, // Value
		{32}, // Header2
		{35}, // Value2
	}

	parts := strings.Split(colorString, ";")
	for _, p := range parts {
		subparts := strings.Split(p, "=")
		idx, e := strconv.Atoi(subparts[0])
		if e != nil {
			continue
		}
		if idx < 0 || idx >= len(colorPatterns) {
			continue
		}
		attrs := strings.Split(subparts[1], ",")
		if len(attrs) == 0 {
			continue
		}
		ii := make([]int, len(attrs))
		for i, attr := range attrs {
			ii[i], e = strconv.Atoi(attr)
			if e != nil {
				ii = nil
				break
			}
		}
		if ii != nil {
			colorPatterns[idx] = ii
		}
	}
}

func prettyPrintBuf(o interface{}) (buf []byte, err error) {
	var v interface{}
	if err := utils.Remarshal(o, &v); err != nil {
		return nil, err
	}

	noColor = noColor || os.Getenv("TERM") == "dumb" || (!isatty.IsTerminal(os.Stdout.Fd()) && !isatty.IsCygwinTerminal(os.Stdout.Fd()))
	processColorPatterns()

	if format == "text" || format == "table" {
		return lamePrinter(v), nil
	}
	return api.PrettyColor(format, v, !noColor, colorPatterns)
}

func prettyPrint(o interface{}) (err error) {
	var buf []byte
	buf, err = prettyPrintBuf(o)
	if err != nil {
		return
	}
	fmt.Println(string(buf))
	if errHaver, ok := o.(models.Validator); ok && objectErrorsAreFatal {
		err = errHaver.HasError()
	}
	return
}

func createFile(fileName string, fileData io.Reader) (copied int64, err error) {
	targetDir := path.Dir(fileName)
	targetName := path.Base(fileName)
	if err = os.MkdirAll(targetDir, 0755); err != nil {
		return
	}
	workingPath := path.Join(targetDir, "."+targetName+".working")
	if err = os.Mkdir(workingPath, 0755); err != nil {
		return
	}
	finalNames := []string{}
	defer os.RemoveAll(workingPath)
	var workingFile *os.File
	if workingFile, err = os.Create(path.Join(workingPath, targetName)); err != nil {
		return
	}
	sum := sha256.New()
	defer workingFile.Close()
	wr := io.MultiWriter(workingFile, sum)
	if copied, err = io.Copy(wr, fileData); err != nil {
		return
	}

	if err = workingFile.Sync(); err != nil {
		return
	}
	var st os.FileInfo
	st, err = workingFile.Stat()
	if err != nil {
		return
	}
	mta := &models.ModTimeSha{
		ModTime: st.ModTime(),
		ShaSum:  sum.Sum(nil),
	}
	if err = mta.SaveToXattr(workingFile); err != nil {
		log.Printf("Error saving etag for %s: %v \n\n", targetName, err)
	}

	var fi *os.File
	fi, err = os.Open(workingPath)
	if err != nil {
		return
	}
	var ents []os.FileInfo
	var srcName, destName string
	ents, err = fi.Readdir(0)
	fi.Close()
	if err != nil {
		return
	}
	for _, nn := range ents {
		name := nn.Name()
		if name == "." || name == ".." {
			continue
		}
		srcName = path.Join(workingPath, name)
		destName = path.Join(targetDir, name)
		if (nn.Mode().Perm() & 0600) != 0600 {
			if err = os.Chmod(srcName, nn.Mode().Perm()|0600); err != nil {
				log.Printf("Error making directory %s writable: %v \n\n", srcName, err)
				continue
			}
		}
		if nn.IsDir() {
			if err = os.RemoveAll(destName); err != nil {
				log.Printf("Error removing target directory%s: %v \n\n", destName, err)
				continue
			}
		}
		if err = os.Rename(srcName, destName); err != nil {
			log.Printf("Error renaming %s to %s: %v \n\n", srcName, destName, err)
			continue
		}
		if nn.Mode()&ignoreMode == 0 {
			finalNames = append(finalNames, destName)
		} else if nn.IsDir() {
			filepath.Walk(destName, func(nnn string, info os.FileInfo, e error) error {
				if e != nil {
					log.Printf("Error walking %s: %v \n", nnn, e)
					return e
				}
				if info.Mode()&ignoreMode == 0 {
					finalNames = append(finalNames, nnn)
				}
				return nil
			})
		}
	}
	return
}

// getChecksum makes a call to provision-server to get the
// checksum of a given file (if it exists)
func getChecksum(fileName, location, path string, local bool) (checksum string) {
	if local {
		fileName = filepath.Join(location, path, fileName)
		stat, err := os.Stat(fileName)
		fi, err := os.Open(fileName)
		if err != nil {
			log.Printf("there was an error opening the file %s %v \n", fileName, err)
			return
		}
		if stat.IsDir() {
			return
		}
		mtime := stat.ModTime()
		mta := &models.ModTimeSha{}
		if err = mta.ReadFromXattr(fi); err != nil {
			return
		}
		if mta != nil && mtime.Equal(mta.ModTime) {
			return mta.String()
		}

		return
	}

	sum, err := Session.GetBlobSum("files", fileName)
	if err == nil {
		checksum = sum
	}
	return
}

func getOs() string {
	if platform != "" {
		split := strings.Split(platform, "/")
		return split[len(split)-1]
	}
	return ""
}

func getArch() string {
	if platform != "" {
		return strings.Split(platform, "/")[0]
	}
	return ""
}

// isValidPlatform checks to see if the given platform is present in the input
// provided by the --platform or -p flag
// any/any is always valid
// platform is a combination of arch and os like arch/os
func isValidPlatform(p string) bool {
	if platform == "" {
		// If no platform is set, we get everything
		return true
	}

	if p == "any/any" {
		return true
	}
	// Check for platform. It can be a comma separated list, so we want to consider all
	platforms := strings.Split(platform, ",")
	for i := range platforms {
		platforms[i] = strings.TrimSpace(platforms[i])

		if platforms[i] == p {
			return true
		}
	}
	return false
}

func getLocalCatalog() (res *models.Content, err error) {
	req := Session.Req().List("files")
	req.Params("path", "rebar-catalog/rackn-catalog")
	data := []interface{}{}
	err = req.Do(&data)
	if err != nil {
		return
	}

	if len(data) == 0 {
		err = fmt.Errorf("failed to find local catalog")
		return
	}

	vs := make([]*semver.Version, len(data))
	vmap := map[string]string{}
	for i, obj := range data {
		r := obj.(string)
		v, verr := semver.NewVersion(strings.TrimSuffix(r, ".json"))
		if verr != nil {
			err = verr
			return
		}
		vs[i] = v
		vmap[v.String()] = r
	}
	sort.Sort(sort.Reverse(semver.Collection(vs)))

	var buf bytes.Buffer
	path := fmt.Sprintf("rebar-catalog/rackn-catalog/%s", vmap[vs[0].String()])
	log.Printf("Using catalog: %s\n", path)
	if gerr := Session.GetBlob(&buf, "files", path); gerr != nil {
		err = fmt.Errorf("Failed to fetch %v: %v: %v", "files", path, gerr)
		return
	}

	err = json.Unmarshal(buf.Bytes(), &res)
	return
}

func itemsFromCatalog(cat *models.Content, name string) map[string]*models.CatalogItem {
	res := map[string]*models.CatalogItem{}
	for k, v := range cat.Sections["catalog_items"] {
		item := &models.CatalogItem{}
		if err := models.Remarshal(v, &item); err != nil {
			continue
		}
		if name == "" || name == item.Name {
			res[k] = item
		}
	}
	return res
}

func lookupVersion(components []interface{}, catalogItem string) string {
	for _, v := range components {
		if v.(map[string]interface{})["Name"] == catalogItem {
			return v.(map[string]interface{})["Version"].(string)
		}
	}
	return ""
}

func isRequired(item *models.CatalogItem) bool {
	// We always want drpcli and dr-provision even if they are not in the version set
	if item.Name == "drpcli" && item.Version == "stable" {
		return true
	}

	if item.Name == "drp" && item.Version == defaultDrpVersion {
		return true
	}

	if defaultUxVersion != "" && (item.Name == "drp-ux" && item.Version == defaultUxVersion) {
		return true
	}

	return false
}

func downloadFile(fileName, fileUrl, baseLocation, path string) error {
	// Check if file already exists, check checksum
	fileLocation := filepath.Join(baseLocation, path, fileName)

	log.Printf("Downloading %s\n", fileUrl)
	data, err := urlOrFileAsReadCloser(fileUrl)
	if err != nil {
		return fmt.Errorf("failed to download file %s due to %v", fileUrl, err)
	}

	log.Printf("Storing %s at %s\n", fileUrl, fileLocation)
	func() {
		defer data.Close()
		_, err = createFile(fileLocation, data)
	}()
	if err != nil {
		return fmt.Errorf("failed to create file %v due to %v", fileName, err)
	}
	return nil
}

// downloadCatalog downloads all the items in the `catalog` to the given location
func downloadCatalog(location string, minVersion string, tip bool, concurrency int, local bool, versionSet string, all bool) error {
	// version = tip is not allowed
	if version == "tip" {
		return fmt.Errorf("invalid value for version. use --tip if you want to include tip versions")
	}

	// if a versionSet has been provided, then load it and get the components
	var components []interface{}
	if versionSet != "" {
		var versionsMap map[string]interface{}
		if err := into(versionSet, &versionsMap); err != nil {
			return fmt.Errorf("Invalid version set: %v\n", err)
		}
		components = versionsMap["Components"].([]interface{})
		if v, ok := versionsMap["DRPVersion"].(string); ok {
			defaultDrpVersion = v
		}
		if v, ok := versionsMap["DRPUXVersion"].(string); ok {
			defaultUxVersion = v
		}
	}

	if concurrency > 20 {
		return fmt.Errorf("invalid value for concurrency: %d. Max allowed is 20", concurrency)
	}

	srcCatalog, err := fetchCatalog()
	if err != nil {
		return err
	}
	srcItems := itemsFromCatalog(srcCatalog, "")

	var localItems map[string]*models.CatalogItem
	if !local {
		localCatalog, err := getLocalCatalog()
		if err == nil {
			localItems = itemsFromCatalog(localCatalog, "")
		}
	}

	requireStable := false
	if minVersion == "stable" {
		requireStable = true
	}

	var mv *semver.Version
	if !requireStable && minVersion != "" {
		var verr error
		mv, verr = semver.NewVersion(minVersion)
		if verr != nil {
			return fmt.Errorf("invalid version: %s, %v", minVersion, verr)
		}
	}

	// If we need to download all items, remove duplicates from srcItems (like stable and tip)
	// srcItemMap.keys() will give you all the IDs we need to download; if there is already a
	// stable it will get override with the actual version and vice versa
	if all {
		filteredSrcItems := make(map[string]*models.CatalogItem)
		srcItemMap := make(map[string]*models.CatalogItem, len(srcItems))
		for _, item := range srcItems {
			srcItemMap[item.Name+item.ActualVersion] = item
		}
		for _, item := range srcItemMap {
			filteredSrcItems[item.Id] = item
		}
		srcItems = filteredSrcItems
	} else { // Filter out all the unwanted src item versions
		filteredSrcItems := make(map[string]*models.CatalogItem)
		srcItemMap := make(map[string]*models.CatalogItem, len(srcItems))
		for k, v := range srcItems {
			if minVersion == "" {
				// We want to get everything here except tip (unless tip is set to true in which case we include tip)
				// We also want to skip stable because we are getting everything here any way so also getting
				// stable will cause some versions to be downloaded twice which will cause problems downstream
				if (!tip && srcItems[k].Tip) || srcItems[k].Version == "stable" {
					continue
				}
			} else if requireStable {
				// Only get things that are stable or tip if tip is set to true
				if !(srcItems[k].Version == "stable" || (tip && srcItems[k].Tip)) {
					continue
				}
			} else {
				// Only get things that aren't stable
				if srcItems[k].Version == "stable" {
					continue
				}
				// Only get versions greater than the input version excluding tip (unless tip is set to true in which case we include tip)
				if mv != nil {
					if o, oerr := semver.NewVersion(srcItems[k].ActualVersion); oerr != nil || mv.Compare(o) > 0 || (!tip && srcItems[k].Tip) {
						continue
					}
				}
			}
			srcItemMap[v.Name+v.ActualVersion] = v
		}
		for _, item := range srcItemMap {
			filteredSrcItems[item.Id] = item
		}
		srcItems = filteredSrcItems
	}

	// We want to be able to process `concurrency` number of updates at a time
	srcItemsKeys := make([]string, 0)
	srcItemsKeyChunks := make([][]string, 0)
	for k := range srcItems {
		srcItemsKeys = append(srcItemsKeys, k)
	}
	// Collect chunks of keys to process
	for i := 0; i < len(srcItemsKeys); i += concurrency {
		end := i + concurrency
		// Avoid out of range
		if end > len(srcItemsKeys) {
			end = len(srcItemsKeys)
		}
		srcItemsKeyChunks = append(srcItemsKeyChunks, srcItemsKeys[i:end])
	}

	var g errgroup.Group
	for _, srcItemsKeys := range srcItemsKeyChunks {
		// Assign temporary variable that is local to this iteration
		srcItemsKeysIteration := srcItemsKeys
		g.Go(func() error {
			for _, k := range srcItemsKeysIteration {
				if versionSet != "" {
					// Make sure that the item exists in the version set and get the required version
					v := lookupVersion(components, srcItems[k].Name)
					if v == "" || srcItems[k].Version != v {
						if !isRequired(srcItems[k]) {
							continue
						}
					}
				}

				// Get things that aren't in local
				nv, ok := localItems[k]
				if !ok || nv.ActualVersion != srcItems[k].ActualVersion {
					parts := map[string]string{}
					i := strings.Index(srcItems[k].Source, "/rebar-catalog/")
					switch srcItems[k].ContentType {
					case "DRP":
						for arch, sum := range srcItems[k].Shasum256 {
							if isValidPlatform(arch) {
								if arch == "any/any" {
									fileName, _ := url.QueryUnescape(srcItems[k].Source[i+1:])
									existingChecksum := getChecksum(fileName, location, "tftpboot/files", local)
									if sum != existingChecksum {
										parts[srcItems[k].Source] = fileName
									}
								} else {
									archValue := strings.Split(arch, "/")[0]
									osValue := strings.Split(arch, "/")[1]
									ts := strings.ReplaceAll(srcItems[k].Source, ".zip", "."+archValue+"."+osValue+".zip")
									qs, _ := url.QueryUnescape(srcItems[k].Source[i+1:])
									td := strings.ReplaceAll(qs, ".zip", "."+archValue+"."+osValue+".zip")
									existingChecksum := getChecksum(td, location, "tftpboot/files", local)
									if sum != existingChecksum {
										parts[ts] = td
									}
								}
							}
						}
					case "PluginProvider", "DRPCLI":
						for arch, sum := range srcItems[k].Shasum256 {
							if isValidPlatform(arch) {
								ts := fmt.Sprintf("%s/%s/%s", srcItems[k].Source, arch, srcItems[k].Name)
								qs, _ := url.QueryUnescape(srcItems[k].Source[i+1:])
								td := fmt.Sprintf("%s/%s/%s", qs, arch, srcItems[k].Name)
								existingChecksum := getChecksum(td, location, "tftpboot/files", local)
								if sum != existingChecksum {
									parts[ts] = td
								}
							}
						}
					default:
						fileName, _ := url.QueryUnescape(srcItems[k].Source[i+1:])
						parts[srcItems[k].Source] = fileName
					}

					for s, d := range parts {
						log.Printf("Downloading %s\n", s)
						data, err := urlOrFileAsReadCloser(s)
						if err != nil {
							return fmt.Errorf("error opening src file %s: %v", s, err)
						}

						if local {
							fileName := filepath.Join(location, "tftpboot/files", d)
							log.Printf("Storing %s at %s\n", s, fileName)
							func() {
								defer data.Close()
								_, err = createFile(fileName, data)
							}()
							if err != nil {
								return generateError(err, "Failed to create file %v", d)
							}
						} else {
							log.Printf("Storing %s at %s\n", s, d)
							func() {
								defer data.Close()
								_, err = Session.PostBlobExplode(data, false, "files", d)
							}()
							if err != nil {
								return generateError(err, "Failed to post %v: %v", "files", d)
							}
						}
					}
				}
			}
			return nil
		})
	}
	if err := g.Wait(); err != nil {
		return err
	}
	return nil
}

func getCatalog(c string) (res *models.Content, err error) {
	buf := []byte{}
	buf, err = bufOrFile(c)
	if err == nil {
		err = json.Unmarshal(buf, &res)
	}
	if err != nil {
		err = fmt.Errorf("error fetching catalog: %v", err)
	}
	var realCatalogSource string
	if catalogSource != "" {
		realCatalogSource = catalogSource
	} else {
		realCatalogSource = api.GetCatalogSource(c, Session)
	}

	// Process catalog items to fix their source
	api.ProcessCatalog(catalog, realCatalogSource, res)
	return
}

// fetchCatalog fetches contents from all catalog urls
// NOTE: if logic here is updated - please also look in api/catalog.go to update similar logic there
// TODO: update so this is common between api and cli packages
func fetchCatalog() (res *models.Content, err error) {
	// Fetch catalog urls
	catalogUrls, err := api.FetchCatalogUrls(Session)
	if err != nil {
		catalogUrls = []string{}
	}
	catalogUrls = append(catalogUrls, catalog)

	var catalogs []*models.Content
	// Loop through all urls and gather all the content
	for _, cUrl := range catalogUrls {
		content, _ := getCatalog(cUrl)
		if content != nil {
			catalogs = append(catalogs, content)
		}
	}

	res, err = api.CombineCatalogs(catalogs)
	return
}
