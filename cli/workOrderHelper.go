package cli

import (
	"fmt"
	"github.com/pborman/uuid"
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func (op *ops) workorders() *cobra.Command {
	workorder := &cobra.Command{
		Use:   "work_order",
		Short: "Access commands for manipulating the work order queue",
	}
	workorder.AddCommand(&cobra.Command{
		Use:   "on [id]",
		Short: "Turn on work order mode for [id]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(models.WorkOrderModer)
			clone.SetWorkOrderMode(true)
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	workorder.AddCommand(&cobra.Command{
		Use:   "off [id]",
		Short: "Turn off work order mode for [id]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(models.WorkOrderModer)
			clone.SetWorkOrderMode(false)
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	workorder.AddCommand(&cobra.Command{
		Use:   "add [id] [json|file|-|template] [params]",
		Short: "Add a work order to the machine [id]",
		Long:  `Using a work order object or template name, create an action and append it to the machine's task list`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires 2 or more arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			aa := &models.WorkOrder{}
			if err = into(args[1], aa); err != nil {
				if args[0] != "-" {
					aa.Blueprint = args[1]
				} else {
					return fmt.Errorf("Unable to create a new work_order: %v", err)
				}
			}
			aa.Machine = uuid.Parse(m.Key())
			if ctx, ok := m.(models.AgentRunner); ok {
				aa.Context = ctx.GetContext()
			}
			answer := &models.WorkOrder{}
			if err = Session.Req().Post(aa).UrlFor(aa.Prefix()).Do(answer); err != nil {
				return generateError(err, "Failed to add workorder to %v: %v", op.singleName, m.Key())
			}
			return prettyPrint(answer)
		},
	})
	return workorder
}
