package models

import (
	"bytes"
	"encoding/json"
	"log"
)

// UxOption defines a potential option that the UX should control.
// UxSetting instantiates the Option.
//
// DEPRECATED:  NOT USED.
//
// swagger:model
type UxOption struct {
	Validation
	Access
	Meta
	Owned
	Bundled

	// Id is the name of the object
	Id string `index:",key"`

	// Documentation is an RST string defining the object
	Documentation string
	// Description is a short string description of the object
	Description string
	// Params is an unused residual of the object from previous releases
	Params map[string]interface{}

	Kind    string
	Global  bool
	Role    bool
	User    bool
	Hidden  bool
	Default string
}

// Key returns the name of the object
func (uo *UxOption) Key() string {
	return uo.Id
}

// KeyName returns the Name field of the Object
func (uo *UxOption) KeyName() string {
	return "Id"
}

// AuthKey returns the field of the Object to use for Auth
func (uo *UxOption) AuthKey() string {
	return uo.Key()
}

// Prefex returns the type of object
func (uo *UxOption) Prefix() string {
	return "ux_options"
}

// GetDocumentaiton returns the object's Documentation
func (uo *UxOption) GetDocumentation() string {
	return uo.Documentation
}

// GetDescription returns the object's Description
func (uo *UxOption) GetDescription() string {
	return uo.Description
}

// Clone the UxOption
func (uo *UxOption) Clone() *UxOption {
	ci2 := &UxOption{}
	buf := bytes.Buffer{}
	enc, dec := json.NewEncoder(&buf), json.NewDecoder(&buf)
	if err := enc.Encode(uo); err != nil {
		log.Panicf("Failed to encode endpoint:%s: %v", uo.Id, err)
	}
	if err := dec.Decode(ci2); err != nil {
		log.Panicf("Failed to decode endpoint:%s: %v", uo.Id, err)
	}
	return ci2
}

// Fill initializes and empty object
func (uo *UxOption) Fill() {
	uo.Validation.fill(uo)
	if uo.Meta == nil {
		uo.Meta = Meta{}
	}
	if uo.Errors == nil {
		uo.Errors = []string{}
	}
	if uo.Params == nil {
		uo.Params = map[string]interface{}{}
	}
}

// SliceOf returns a slice of objects
func (uo *UxOption) SliceOf() interface{} {
	s := []*UxOption{}
	return &s
}

// ToModels converts a Slice of objects into a list of Model
func (uo *UxOption) ToModels(obj interface{}) []Model {
	items := obj.(*[]*UxOption)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// CanHaveActions says that actions can be added to this object type
func (uo *UxOption) CanHaveActions() bool {
	return true
}

// SetName sets the name. In this case, it sets Id.
func (uo *UxOption) SetName(name string) {
	uo.Id = name
}
