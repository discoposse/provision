package models

import (
	"bytes"
	"encoding/json"
	"log"
)

// Classifier is deprecated
// swagger:model
type Classifier struct {
	Test        string
	Title       string
	Icon        string
	Regex       string
	Placeholder string
	NoAppend    bool
	Continue    bool
	Params      map[string]interface{}
}

// MenuItem defines a new navigation menu.
// swagger:model
type MenuItem struct {
	Id        string `json:",omitempty"`
	Title     string `json:",omitempty"`
	Filter    string `json:",omitempty"`
	To        string `json:",omitempty"`
	Overwrite string `json:",omitempty"`
	Icon      string `json:",omitempty"`
	Color     string `json:",omitempty"`
}

// MenuItem defines a new navigation menu.
// swagger:model
type MenuGroup struct {
	Id    string     `json:",omitempty"`
	Title string     `json:",omitempty"`
	Items []MenuItem `json:",omitempty"`
}

// UxView structure that handles RawModel instead of dealing with
// RawModel which is how DRP is storing it.
//
// swagger:model
type UxView struct {
	Validation
	Access
	Meta
	Owned
	Bundled

	// Id is the Name of the Filter
	Id string `index:",key"`

	// Documentation is an RST string defining the object
	Documentation string
	// Description is a short string description of the object
	Description string
	// Params is an unused residual of the object from previous releases
	Params map[string]interface{}

	// ApplicableRoles defines the roles that this view shows up for.
	// e.g. superuser means that it will be available for users with the superuser role.
	ApplicableRoles []string
	// Airgap is not used.  Moved to license.
	// Deprecated
	Airgap bool
	// LandingPage defines the default navigation route
	// None or "" will open the system page.
	// if it starts with http, it will navigate to the Overiew page.
	// Otherwise, it will go to the machine's page.
	LandingPage string
	// ShowActiviation is not used.  Moved to license.
	// Deprecated
	ShowActiviation bool
	// BrandingImage defines a files API path that should point to an image file.
	// This replaces the RackN logo.
	BrandingImage string
	// Menu defines the menu elements.
	Menu []MenuGroup

	MachineFields []string
	BulkTabs      []string
	// Columns defines the custom colums for a MenuItem Id
	Columns              map[string][]string
	HideEditObjects      []string
	WorkflowsRestriction []string
	StagesRestriction    []string
	TasksRestriction     []string
	ProfilesRestriction  []string
	ParamsRestriction    []string
	// Classifiers is deprecated
	Classifiers []Classifier
}

// Key returns the name of the object
func (ux *UxView) Key() string {
	return ux.Id
}

// KeyName returns the Name field of the Object
func (ux *UxView) KeyName() string {
	return "Id"
}

// AuthKey returns the field of the Object to use for Auth
func (ux *UxView) AuthKey() string {
	return ux.Key()
}

// Prefex returns the type of object
func (ux *UxView) Prefix() string {
	return "ux_views"
}

// GetDocumentaiton returns the object's Documentation
func (ux *UxView) GetDocumentation() string {
	return ux.Documentation
}

// GetDescription returns the object's Description
func (ux *UxView) GetDescription() string {
	return ux.Description
}

// Clone the UxView
func (ux *UxView) Clone() *UxView {
	ci2 := &UxView{}
	buf := bytes.Buffer{}
	enc, dec := json.NewEncoder(&buf), json.NewDecoder(&buf)
	if err := enc.Encode(ux); err != nil {
		log.Panicf("Failed to encode endpoint:%s: %v", ux.Id, err)
	}
	if err := dec.Decode(ci2); err != nil {
		log.Panicf("Failed to decode endpoint:%s: %v", ux.Id, err)
	}
	return ci2
}

// Fill initializes and empty object
func (ux *UxView) Fill() {
	ux.Validation.fill(ux)
	if ux.Meta == nil {
		ux.Meta = Meta{}
	}
	if ux.Errors == nil {
		ux.Errors = []string{}
	}
	if ux.Params == nil {
		ux.Params = map[string]interface{}{}
	}
	if ux.Menu == nil {
		ux.Menu = []MenuGroup{}
	}
	if ux.MachineFields == nil {
		ux.MachineFields = []string{}
	}
	if ux.BulkTabs == nil {
		ux.BulkTabs = []string{}
	}
	if ux.Columns == nil {
		ux.Columns = map[string][]string{}
	}
	if ux.HideEditObjects == nil {
		ux.HideEditObjects = []string{}
	}
	if ux.WorkflowsRestriction == nil {
		ux.WorkflowsRestriction = []string{}
	}
	if ux.StagesRestriction == nil {
		ux.StagesRestriction = []string{}
	}
	if ux.TasksRestriction == nil {
		ux.TasksRestriction = []string{}
	}
	if ux.ProfilesRestriction == nil {
		ux.ProfilesRestriction = []string{}
	}
	if ux.ParamsRestriction == nil {
		ux.ParamsRestriction = []string{}
	}
	if ux.Classifiers == nil {
		ux.Classifiers = []Classifier{}
	}
}

// SliceOf returns a slice of objects
func (ux *UxView) SliceOf() interface{} {
	s := []*UxView{}
	return &s
}

// ToModels converts a Slice of objects into a list of Model
func (ux *UxView) ToModels(obj interface{}) []Model {
	items := obj.(*[]*UxView)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// CanHaveActions says that actions can be added to this object type
func (ux *UxView) CanHaveActions() bool {
	return true
}

// SetName sets the name. In this case, it sets Id.
func (ux *UxView) SetName(name string) {
	ux.Id = name
}
