.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license

.. REFERENCE kb-00000 for an example and information on how to use this template.
.. If you make EDITS - ensure you update footer release date information.
.. Generated from https://gitlab.com/rackn/provision/-/blob/v4/tools/docs-make-kb.sh


.. _rs_kb_00003:
.. _rs_bind_error:

kb-00003: Bind Error
~~~~~~~~~~~~~~~~~~~~

Knowledge Base Article: kb-00003


Description
-----------

The Digital Rebar Provision service (``dr-provision``) failed to start up.  I received an
error about *address already in use*.


Solution
--------

Digital Rebar Provision will fail if it cannot attach to one of the required ports.

* Typical Error Messages:

  * Digital Rebar fails with `listen udp4 :67: bind: address already in use`
  * Installer fails preflight check with `The following ports are in use`

* Additional Information: The conflicted port will be included in the error between colons (e.g.: `:67:`)
* Investigate by using `sudo netstat -ua` to list all UDP ports in use by your system; however, you may have conflicts without showing listener!
* Workaround: If the conflicting service is not required, simply disable that service
* Resolution: Stop the offending service on the system.  Typical corrective actions are:

  * 67 - dhcp.  Correct with `sudo pkill dnsmasq`
  * Remove the DHCP elements from libvirt (see below)

See the :ref:`rs_arch_ports` mapping list on start-up of the ``dr-provision`` service for a complete list.

Once ``dr-provision`` is running, you can obtain a list of the ports that it's holding open
with the following CLI command:

* ``drpcli info status``

Additional Information
----------------------

While stopping dnsmasq will address the immediate concern, operators should determine and disable
the service(s) that are conflicting since they are likely to cause issues to reoccure.

Disable Libvirt
===============

If Libvirt is conficting, you may choose to simply distable Libvirt and kill dnsmasq.

`sudo systemctl disable --now libvirtd && sudo pkill dnsmasq`

Disable DHCP in Libvirt
=======================

Instead of disabling Libvirt, operators may choose to remove the DHCP service from
Libvirt and keep it running otherwise.

  ::

    # if you don't have virsh, you'll need to install it
    sudo yum install -y libvirt-client

    # find the active networks (lkely default)
    sudo virsh net-list
    # in editor - remove all dhcp elements from <ip> section
    sudo virsh net-edit <nets - generally default>

    # remove the network
    sudo virsh net-destroy <nets - generally default>
    # reset the system
    sudo virsh net-start <nets - generally default>

See Also
========

Digital Rebar uses the following network :ref:`rs_arch_ports`.

Versions
========

all

Keywords
========

dhcp, bind address, port, startup, dnsmasq, libvirt


Revision Information
====================
  ::

    KB Article     :  kb-00003
    initial release:  Wed Jun 10 09:40:47 PDT 2020
    updated release:  Wed Jun 10 09:40:47 PDT 2020
    updated release:  July 18, 2022

