.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license

.. REFERENCE kb-00000 for an example and information on how to use this template.
.. If you make EDITS - ensure you update footer release date information.
.. Generated from https://gitlab.com/rackn/provision/-/blob/v4/tools/docs-make-kb.sh


.. _using_isofs_to_save_space:
.. _rs_kb_00079:

kb-00079: Using ISOFS To Save Space
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Knowledge Base Article: kb-00079


Description
-----------
How a DRP operator can use the new ISOFS feature in DRP to save disk space on the DRP endpoint

Solution
--------
In v4.11 a new feature was added to dr-provision allowing operators to save space on the DRP endpoint called ISOFS.
ISOFS is enabled by default.  It presents the contents of the ISO or uncompressed tarball on-demand.
This eliminates the need to store the exploded contents directly on disk.

ISOFS doesn't remove previously exploded ISO content. To take advantage of the feature for an already extracted
ISO be sure the ISO file is available in the $RS_BASE_ROOT/tftpboot/isos directory. Next delete the extracted ISO folder
located in the $RS_BASE_ROOT/tftpboot directory. Next you may need to restart DRP to trigger the process to run.

Any new ISO files added will automatically be handled by ISOFS if they are supported. If you have problems with an ISO not
being supported and find it is being extracted please make sure you are using at least v4.11 before you open a ticket with support.


Additional Information
----------------------

If you disable ISOFS and do not have your ISOs extracted, when DRP is started it may appear to be unresponsive.
This is likely due to DRP extracting all the ISOs. You can check `top` to see if the process is running, as well as review the dr-provision
logs using `journalctl -f -u dr-provision`. Depending on the number of ISOs being exploded and the stats of the
machine the process may take several minutes.


See Also
========


Versions
========
v4.11 and newer

Keywords
========
isofs, extract iso

Revision Information
====================
  ::

    KB Article     :  kb-00079
    initial release:  Mon 07 Nov 2022 03:42:10 PM CST
    updated release:  Mon 07 Nov 2022 03:42:10 PM CST

