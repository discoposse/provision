.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. Generated from https://gitlab.com/rackn/provision/-/blob/v4/tools/docs-make-kb.sh


.. _proxy_allow_list_kb:
.. _rs_kb_00072:

kb-00072: Proxy Allow List
~~~~~~~~~~~~~~~~~~~~~~~~~~

Knowledge Base Article: kb-00072


Proxy Allow List
----------------

This document provides a comprehensive lists of domains that should be included in a proxy's allow list.


URL Lists
---------

While we do not specifically detail how to create the individual allow lists, we do provide the following URLs that can be used.  We do not include external sites that may be used for components such as distribution ISOs pulled from bootenvs, etc.

Catalog
=======

Catalog URLs provide metadata and files that are used to install/upgrade components of Digital Rebar.  Access to these URLs is not needed for normal operation.

.. code::

  https://rebar-catalog.s3-us-west-2.amazonaws.com
  https://rebar-catalog.s3.us-west-2.amazonaws.com
  https://s3-us-west-2.amazonaws.com/rebar-catalog

Repos
=====

Repo URLs provide storage for files needed Digital Rebar and it's components.  Access to these URLs is not needed for normal operation.

.. code::

  https://get.rebar.digital
  https://gitlab.com/rackn
  https://rackn-repo.s3.amazonaws.com
  https://rackn-repo.s3-us-west-2.amazonaws.com
  https://rackn-repo.s3.us-west-2.amazonaws.com
  https://rackn-sledgehammer.s3.amazonaws.com
  https://rackn-sledgehammer.s3-us-west-2.amazonaws.com
  https://rebar.digital
  https://repo.rackn.io
  https://s3-us-west-2.amazonaws.com/get.rebar.digital
  https://s3-us-west-2.amazonaws.com/rackn-sledgehammer

UX
===

UX URLs provide UX interface.  Unless you are relying only on API, the CLI, or you have licensed airgap, you will need this for management of Digital Rebar.

.. code::

  https://portal.rackn.io
  https://tip.rackn.io
  https://rackn-ux.s3-us-west-2.amazonaws.com


Licensing
=========

URL used to communicate license information.  This is normally needed for license renewals.

.. code::

  https://cloudia.rackn.io
  https://cloudia2.rackn.io

Support
=======

Support URLs aren't needed for regular operation, but are good to have available for support purposes.

.. code::

  https://provision.readthedocs.io
  https://rackn.com
  https://rackn-support-dumps.s3.amazonaws.com
  https://rackn.zendesk.com
  https://readthedocs.org/projects/provision
  https://doc.rackn.io


General
=======

Although specific sub-domains are already included, the following may be be a good wildcard addition to simplify lists.

.. code::

  https://*.rackn.io

See Also
========

  * :ref:`rs_arch_ports`


Versions
========

This document applies to all version of Digital Rebar.


Keywords
========

proxy


Revision Information
====================
  ::

    KB Article     :  kb-00072
    initial release:  Wed Jan 12 13:48:00 EST 2022
    updated release:  Wed Jan 12 13:48:00 EST 2022

