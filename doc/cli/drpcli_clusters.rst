
.. _rs_drpcli_clusters:

drpcli clusters
---------------

Access CLI commands relating to clusters

Options
~~~~~~~

::

     -h, --help   help for clusters

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli <drpcli.html>`__ - A CLI application for interacting with the
   DigitalRebar Provision API
-  `drpcli clusters action <drpcli_clusters_action.html>`__ - Display
   the action for this cluster
-  `drpcli clusters actions <drpcli_clusters_actions.html>`__ - Display
   actions for this cluster
-  `drpcli clusters add <drpcli_clusters_add.html>`__ - Add the clusters
   param *key* to *blob*
-  `drpcli clusters addprofile <drpcli_clusters_addprofile.html>`__ -
   Add profile to the cluster’s profile list
-  `drpcli clusters addtask <drpcli_clusters_addtask.html>`__ - Add task
   to the cluster’s task list
-  `drpcli clusters await <drpcli_clusters_await.html>`__ - Wait for a
   cluster’s field to become a value within a number of seconds
-  `drpcli clusters bootenv <drpcli_clusters_bootenv.html>`__ - Set the
   cluster’s bootenv
-  `drpcli clusters cleanup <drpcli_clusters_cleanup.html>`__ - Cleanup
   cluster by id
-  `drpcli clusters count <drpcli_clusters_count.html>`__ - Count all
   clusters
-  `drpcli clusters create <drpcli_clusters_create.html>`__ - Create a
   new cluster with the passed-in JSON or string key
-  `drpcli clusters currentlog <drpcli_clusters_currentlog.html>`__ -
   Get the log for the most recent job run on the cluster
-  `drpcli clusters deletejobs <drpcli_clusters_deletejobs.html>`__ -
   Delete all jobs associated with cluster
-  `drpcli clusters destroy <drpcli_clusters_destroy.html>`__ - Destroy
   cluster by id
-  `drpcli clusters etag <drpcli_clusters_etag.html>`__ - Get the etag
   for a clusters by id
-  `drpcli clusters exists <drpcli_clusters_exists.html>`__ - See if a
   clusters exists by id
-  `drpcli clusters get <drpcli_clusters_get.html>`__ - Get a parameter
   from the cluster
-  `drpcli clusters group <drpcli_clusters_group.html>`__ - Commands to
   control parameters on the group profile
-  `drpcli clusters indexes <drpcli_clusters_indexes.html>`__ - Get
   indexes for clusters
-  `drpcli clusters inserttask <drpcli_clusters_inserttask.html>`__ -
   Insert a task at [offset] from cluster’s running task
-  `drpcli clusters inspect <drpcli_clusters_inspect.html>`__ - Commands
   to inspect tasks and jobs on machines
-  `drpcli clusters jobs <drpcli_clusters_jobs.html>`__ - Access
   commands for manipulating the current job
-  `drpcli clusters list <drpcli_clusters_list.html>`__ - List all
   clusters
-  `drpcli clusters meta <drpcli_clusters_meta.html>`__ - Gets metadata
   for the cluster
-  `drpcli clusters params <drpcli_clusters_params.html>`__ - Gets/sets
   all parameters for the cluster
-  `drpcli clusters patch <drpcli_clusters_patch.html>`__ - Patch
   cluster by ID using the passed-in JSON Patch
-  `drpcli clusters pause <drpcli_clusters_pause.html>`__ - Mark the
   cluster as NOT runnable
-  `drpcli clusters processjobs <drpcli_clusters_processjobs.html>`__ -
   For the given cluster, process pending jobs until done.
-  `drpcli clusters
   releaseToPool <drpcli_clusters_releaseToPool.html>`__ - Release this
   cluster back to the pool
-  `drpcli clusters remove <drpcli_clusters_remove.html>`__ - Remove the
   param *key* from clusters
-  `drpcli clusters
   removeprofile <drpcli_clusters_removeprofile.html>`__ - Remove a
   profile from the cluster’s profile list
-  `drpcli clusters removetask <drpcli_clusters_removetask.html>`__ -
   Remove a task from the cluster’s list
-  `drpcli clusters run <drpcli_clusters_run.html>`__ - Mark the cluster
   as runnable
-  `drpcli clusters runaction <drpcli_clusters_runaction.html>`__ - Run
   action on object from plugin
-  `drpcli clusters set <drpcli_clusters_set.html>`__ - Set the clusters
   param *key* to *blob*
-  `drpcli clusters show <drpcli_clusters_show.html>`__ - Show a single
   clusters by id
-  `drpcli clusters stage <drpcli_clusters_stage.html>`__ - Set the
   cluster’s stage
-  `drpcli clusters start <drpcli_clusters_start.html>`__ - Start the
   cluster’s workflow
-  `drpcli clusters tasks <drpcli_clusters_tasks.html>`__ - Access task
   manipulation for cluster
-  `drpcli clusters update <drpcli_clusters_update.html>`__ - Unsafely
   update cluster by id with the passed-in JSON
-  `drpcli clusters uploadiso <drpcli_clusters_uploadiso.html>`__ - This
   will attempt to upload the ISO from the specified ISO URL.
-  `drpcli clusters wait <drpcli_clusters_wait.html>`__ - Wait for a
   cluster’s field to become a value within a number of seconds
-  `drpcli clusters whoami <drpcli_clusters_whoami.html>`__ - Figure out
   what cluster UUID most closely matches the current system
-  `drpcli clusters work_order <drpcli_clusters_work_order.html>`__ -
   Access commands for manipulating the work order queue
-  `drpcli clusters workflow <drpcli_clusters_workflow.html>`__ - Set
   the cluster’s workflow
