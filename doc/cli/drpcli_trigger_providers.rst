
.. _rs_drpcli_trigger_providers:

drpcli trigger_providers
------------------------

Access CLI commands relating to trigger_providers

Options
~~~~~~~

::

     -h, --help   help for trigger_providers

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli <drpcli.html>`__ - A CLI application for interacting with the
   DigitalRebar Provision API
-  `drpcli trigger_providers
   action <drpcli_trigger_providers_action.html>`__ - Display the action
   for this trigger_provider
-  `drpcli trigger_providers
   actions <drpcli_trigger_providers_actions.html>`__ - Display actions
   for this trigger_provider
-  `drpcli trigger_providers add <drpcli_trigger_providers_add.html>`__
   - Add the trigger_providers param *key* to *blob*
-  `drpcli trigger_providers
   addprofile <drpcli_trigger_providers_addprofile.html>`__ - Add
   profile to the trigger_provider’s profile list
-  `drpcli trigger_providers
   await <drpcli_trigger_providers_await.html>`__ - Wait for a
   trigger_provider’s field to become a value within a number of seconds
-  `drpcli trigger_providers
   count <drpcli_trigger_providers_count.html>`__ - Count all
   trigger_providers
-  `drpcli trigger_providers
   create <drpcli_trigger_providers_create.html>`__ - Create a new
   trigger_provider with the passed-in JSON or string key
-  `drpcli trigger_providers
   destroy <drpcli_trigger_providers_destroy.html>`__ - Destroy
   trigger_provider by id
-  `drpcli trigger_providers
   etag <drpcli_trigger_providers_etag.html>`__ - Get the etag for a
   trigger_providers by id
-  `drpcli trigger_providers
   exists <drpcli_trigger_providers_exists.html>`__ - See if a
   trigger_providers exists by id
-  `drpcli trigger_providers get <drpcli_trigger_providers_get.html>`__
   - Get a parameter from the trigger_provider
-  `drpcli trigger_providers
   indexes <drpcli_trigger_providers_indexes.html>`__ - Get indexes for
   trigger_providers
-  `drpcli trigger_providers
   list <drpcli_trigger_providers_list.html>`__ - List all
   trigger_providers
-  `drpcli trigger_providers
   meta <drpcli_trigger_providers_meta.html>`__ - Gets metadata for the
   trigger_provider
-  `drpcli trigger_providers
   params <drpcli_trigger_providers_params.html>`__ - Gets/sets all
   parameters for the trigger_provider
-  `drpcli trigger_providers
   patch <drpcli_trigger_providers_patch.html>`__ - Patch
   trigger_provider by ID using the passed-in JSON Patch
-  `drpcli trigger_providers
   remove <drpcli_trigger_providers_remove.html>`__ - Remove the param
   *key* from trigger_providers
-  `drpcli trigger_providers
   removeprofile <drpcli_trigger_providers_removeprofile.html>`__ -
   Remove a profile from the trigger_provider’s profile list
-  `drpcli trigger_providers
   runaction <drpcli_trigger_providers_runaction.html>`__ - Run action
   on object from plugin
-  `drpcli trigger_providers set <drpcli_trigger_providers_set.html>`__
   - Set the trigger_providers param *key* to *blob*
-  `drpcli trigger_providers
   show <drpcli_trigger_providers_show.html>`__ - Show a single
   trigger_providers by id
-  `drpcli trigger_providers
   update <drpcli_trigger_providers_update.html>`__ - Unsafely update
   trigger_provider by id with the passed-in JSON
-  `drpcli trigger_providers
   uploadiso <drpcli_trigger_providers_uploadiso.html>`__ - This will
   attempt to upload the ISO from the specified ISO URL.
-  `drpcli trigger_providers
   wait <drpcli_trigger_providers_wait.html>`__ - Wait for a
   trigger_provider’s field to become a value within a number of seconds
