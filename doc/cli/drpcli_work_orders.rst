
.. _rs_drpcli_work_orders:

drpcli work_orders
------------------

Access CLI commands relating to work_orders

Options
~~~~~~~

::

     -h, --help   help for work_orders

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli <drpcli.html>`__ - A CLI application for interacting with the
   DigitalRebar Provision API
-  `drpcli work_orders action <drpcli_work_orders_action.html>`__ -
   Display the action for this work_order
-  `drpcli work_orders actions <drpcli_work_orders_actions.html>`__ -
   Display actions for this work_order
-  `drpcli work_orders add <drpcli_work_orders_add.html>`__ - Add the
   work_orders param *key* to *blob*
-  `drpcli work_orders
   addprofile <drpcli_work_orders_addprofile.html>`__ - Add profile to
   the work_order’s profile list
-  `drpcli work_orders addtask <drpcli_work_orders_addtask.html>`__ -
   Add task to the work_order’s task list
-  `drpcli work_orders await <drpcli_work_orders_await.html>`__ - Wait
   for a work_order’s field to become a value within a number of seconds
-  `drpcli work_orders count <drpcli_work_orders_count.html>`__ - Count
   all work_orders
-  `drpcli work_orders create <drpcli_work_orders_create.html>`__ -
   Create a new work_order with the passed-in JSON or string key
-  `drpcli work_orders destroy <drpcli_work_orders_destroy.html>`__ -
   Destroy work_order by id
-  `drpcli work_orders etag <drpcli_work_orders_etag.html>`__ - Get the
   etag for a work_orders by id
-  `drpcli work_orders exists <drpcli_work_orders_exists.html>`__ - See
   if a work_orders exists by id
-  `drpcli work_orders get <drpcli_work_orders_get.html>`__ - Get a
   parameter from the work_order
-  `drpcli work_orders indexes <drpcli_work_orders_indexes.html>`__ -
   Get indexes for work_orders
-  `drpcli work_orders
   inserttask <drpcli_work_orders_inserttask.html>`__ - Insert a task at
   [offset] from work_order’s running task
-  `drpcli work_orders list <drpcli_work_orders_list.html>`__ - List all
   work_orders
-  `drpcli work_orders meta <drpcli_work_orders_meta.html>`__ - Gets
   metadata for the work_order
-  `drpcli work_orders params <drpcli_work_orders_params.html>`__ -
   Gets/sets all parameters for the work_order
-  `drpcli work_orders patch <drpcli_work_orders_patch.html>`__ - Patch
   work_order by ID using the passed-in JSON Patch
-  `drpcli work_orders purge <drpcli_work_orders_purge.html>`__ - Purge
   work_orders in excess of the work_order retention preferences
-  `drpcli work_orders remove <drpcli_work_orders_remove.html>`__ -
   Remove the param *key* from work_orders
-  `drpcli work_orders
   removeprofile <drpcli_work_orders_removeprofile.html>`__ - Remove a
   profile from the work_order’s profile list
-  `drpcli work_orders
   removetask <drpcli_work_orders_removetask.html>`__ - Remove a task
   from the work_order’s list
-  `drpcli work_orders run <drpcli_work_orders_run.html>`__ - Run a new
   work_order using [templateName] on [machineID]
-  `drpcli work_orders runaction <drpcli_work_orders_runaction.html>`__
   - Run action on object from plugin
-  `drpcli work_orders set <drpcli_work_orders_set.html>`__ - Set the
   work_orders param *key* to *blob*
-  `drpcli work_orders show <drpcli_work_orders_show.html>`__ - Show a
   single work_orders by id
-  `drpcli work_orders tasks <drpcli_work_orders_tasks.html>`__ - Access
   task manipulation for machines
-  `drpcli work_orders update <drpcli_work_orders_update.html>`__ -
   Unsafely update work_order by id with the passed-in JSON
-  `drpcli work_orders uploadiso <drpcli_work_orders_uploadiso.html>`__
   - This will attempt to upload the ISO from the specified ISO URL.
-  `drpcli work_orders wait <drpcli_work_orders_wait.html>`__ - Wait for
   a work_order’s field to become a value within a number of seconds
