
.. _rs_drpcli_identity_providers:

drpcli identity_providers
-------------------------

Access CLI commands relating to identity_providers

Options
~~~~~~~

::

     -h, --help   help for identity_providers

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli <drpcli.html>`__ - A CLI application for interacting with the
   DigitalRebar Provision API
-  `drpcli identity_providers
   action <drpcli_identity_providers_action.html>`__ - Display the
   action for this identity_provider
-  `drpcli identity_providers
   actions <drpcli_identity_providers_actions.html>`__ - Display actions
   for this identity_provider
-  `drpcli identity_providers
   await <drpcli_identity_providers_await.html>`__ - Wait for a
   identity_provider’s field to become a value within a number of
   seconds
-  `drpcli identity_providers
   count <drpcli_identity_providers_count.html>`__ - Count all
   identity_providers
-  `drpcli identity_providers
   create <drpcli_identity_providers_create.html>`__ - Create a new
   identity_provider with the passed-in JSON or string key
-  `drpcli identity_providers
   destroy <drpcli_identity_providers_destroy.html>`__ - Destroy
   identity_provider by id
-  `drpcli identity_providers
   etag <drpcli_identity_providers_etag.html>`__ - Get the etag for a
   identity_providers by id
-  `drpcli identity_providers
   exists <drpcli_identity_providers_exists.html>`__ - See if a
   identity_providers exists by id
-  `drpcli identity_providers
   indexes <drpcli_identity_providers_indexes.html>`__ - Get indexes for
   identity_providers
-  `drpcli identity_providers
   list <drpcli_identity_providers_list.html>`__ - List all
   identity_providers
-  `drpcli identity_providers
   meta <drpcli_identity_providers_meta.html>`__ - Gets metadata for the
   identity_provider
-  `drpcli identity_providers
   patch <drpcli_identity_providers_patch.html>`__ - Patch
   identity_provider by ID using the passed-in JSON Patch
-  `drpcli identity_providers
   runaction <drpcli_identity_providers_runaction.html>`__ - Run action
   on object from plugin
-  `drpcli identity_providers
   show <drpcli_identity_providers_show.html>`__ - Show a single
   identity_providers by id
-  `drpcli identity_providers
   update <drpcli_identity_providers_update.html>`__ - Unsafely update
   identity_provider by id with the passed-in JSON
-  `drpcli identity_providers
   wait <drpcli_identity_providers_wait.html>`__ - Wait for a
   identity_provider’s field to become a value within a number of
   seconds
