{
  "$defs": {
    "Meta": {
      "description": "Meta holds information about arbitrary things.",
      "patternProperties": {
        ".*": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "Stage": {
      "additionalProperties": false,
      "description": "Stage encapsulates a set of tasks and profiles to apply to a Machine in a BootEnv.",
      "properties": {
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "BootEnv": {
          "description": "The BootEnv the machine should be in to run this stage.\nIf the machine is not in this bootenv, the bootenv of the\nmachine will be changed.\n\nrequired: true",
          "type": "string"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Description": {
          "description": "A description of this stage.  This should tell what it is for,\nany special considerations that should be taken into account when\nusing it, etc.",
          "type": "string"
        },
        "Documentation": {
          "description": "Documentation of this stage.  This should tell what\nthe stage is for, any special considerations that\nshould be taken into account when using it, etc. in rich structured text (rst).",
          "type": "string"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "Name": {
          "description": "The name of the stage.\n\nrequired: true",
          "type": "string"
        },
        "OptionalParams": {
          "description": "The list of extra optional parameters for this\nstage. They can be present as Machine.Params when\nthe stage is applied to the machine.  These are more\nother consumers of the stage to know what parameters\ncould additionally be applied to the stage by the\nrenderer based upon the Machine.Params",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "OutputParams": {
          "description": "OutputParams are that parameters that are possibly set by the Task",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Params": {
          "description": "Params contains parameters for the stage.\nThis allows the machine to access these values while in this stage.",
          "type": "object"
        },
        "Partial": {
          "description": "Partial tracks if the object is not complete when returned.\nread only: true",
          "type": "boolean"
        },
        "Profiles": {
          "description": "The list of profiles a machine should use while in this stage.\nThese are used after machine profiles, but before global.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "Reboot": {
          "description": "Flag to indicate if a node should be PXE booted on this\ntransition into this Stage.  The nextbootpxe and reboot\nmachine actions will be called if present and Reboot is true",
          "type": "boolean"
        },
        "RequiredParams": {
          "description": "The list of extra required parameters for this\nstage. They should be present as Machine.Params when\nthe stage is applied to the machine.\n\nrequired: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "RunnerWait": {
          "description": "This flag is deprecated and will always be TRUE.",
          "type": "boolean"
        },
        "Tasks": {
          "description": "The list of initial machine tasks that the stage should run",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Templates": {
          "description": "The templates that should be expanded into files for the stage.\n\nrequired: true",
          "items": {
            "$ref": "#/$defs/TemplateInfo"
          },
          "type": "array"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        }
      },
      "type": "object"
    },
    "TemplateInfo": {
      "additionalProperties": false,
      "description": "TemplateInfo holds information on the templates in the boot environment that will be expanded into files.",
      "properties": {
        "Contents": {
          "description": "Contents that should be used when this template needs\nto be expanded.  Either this or ID should be set.\n\nrequired: false",
          "type": "string"
        },
        "ID": {
          "description": "ID of the template that should be expanded.  Either\nthis or Contents should be set\n\nrequired: false",
          "type": "string"
        },
        "Link": {
          "description": "Link optionally references another file to put at\nthe path location.",
          "type": "string"
        },
        "Meta": {
          "description": "Meta for the TemplateInfo.  This can be used by the job running\nsystem and the bootenvs to handle OS, arch, and firmware differences.\n\nrequired: false",
          "patternProperties": {
            ".*": {
              "type": "string"
            }
          },
          "type": "object"
        },
        "Name": {
          "description": "Name of the template\n\nrequired: true",
          "type": "string"
        },
        "Path": {
          "description": "A text/template that specifies how to create\nthe final path the template should be\nwritten to.\n\nrequired: true",
          "type": "string"
        }
      },
      "type": "object"
    }
  },
  "$id": "https://gitlab.com/rackn/provision/v4/models/stage",
  "$ref": "#/$defs/Stage",
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}
