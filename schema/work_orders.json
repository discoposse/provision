{
  "$defs": {
    "Meta": {
      "description": "Meta holds information about arbitrary things.",
      "patternProperties": {
        ".*": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "TaskStack": {
      "additionalProperties": false,
      "description": "TaskStack contains a stack of tasks.",
      "properties": {
        "CurrentTask": {
          "type": "integer"
        },
        "TaskList": {
          "items": {
            "type": "string"
          },
          "type": "array"
        }
      },
      "type": "object"
    },
    "UUID": {
      "contentEncoding": "base64",
      "type": "string"
    },
    "WorkOrder": {
      "additionalProperties": false,
      "description": "WorkOrder is custom workflow-like element that defines parameters and an Blueprint that acts as a basis for expanding into tasks.",
      "properties": {
        "Archived": {
          "description": "Archived indicates whether the complete log for the async action can be\nretrieved via the API.  If Archived is true, then the log cannot\nbe retrieved.\n\nrequired: true",
          "type": "boolean"
        },
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "Blueprint": {
          "description": "Blueprint defines the tasks and base parameters for this action\nrequired: true",
          "type": "string"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Context": {
          "description": "Contexts contains the name of the current execution context.\nAn empty string indicates that an agent running on a Machine should be executing tasks,\nand any other value means that an agent running with its context set for this value should\nbe executing tasks.",
          "type": "string"
        },
        "CreateTime": {
          "description": "CreateTime is the time the work order was created.  This is\ndistinct from StartTime, as there may be a significant delay before\nthe workorder starts running.",
          "format": "date-time",
          "type": "string"
        },
        "CurrentJob": {
          "$ref": "#/$defs/UUID",
          "description": "The UUID of the job that is currently running.\n\nswagger:strfmt uuid"
        },
        "CurrentTask": {
          "description": "The index into the Tasks list for the task that is currently\nrunning (if a task is running) or the next task that will run (if\nno task is currently running).  If -1, then the first task will\nrun next, and if it is equal to the length of the Tasks list then\nall the tasks have finished running.\n\nrequired: true",
          "type": "integer"
        },
        "EndTime": {
          "description": "EndTime The time the async action failed or finished or cancelled.",
          "format": "date-time",
          "type": "string"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Filter": {
          "description": "Filter is a list filter for this WorkOrder",
          "type": "string"
        },
        "Machine": {
          "$ref": "#/$defs/UUID",
          "description": "Machine is the key of the machine running the WorkOrder\nswagger:strfmt uuid"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "Params": {
          "description": "Params that have been directly set on the Machine.",
          "type": "object"
        },
        "Profiles": {
          "description": "Profiles An array of profiles to apply to this machine in order when looking\nfor a parameter during rendering.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "RetryTaskAttempt": {
          "description": "This tracks the number of retry attempts for the current task.\nWhen a task succeeds, the retry value is reset.",
          "type": "integer"
        },
        "Runnable": {
          "description": "Runnable indicates that this is Runnable.",
          "type": "boolean"
        },
        "Stage": {
          "description": "The stage that this is currently in.",
          "type": "string"
        },
        "StartTime": {
          "description": "StartTime The time the async action started running.",
          "format": "date-time",
          "type": "string"
        },
        "State": {
          "description": "State The state the async action is in.  Must be one of \"created\", \"running\", \"failed\", \"finished\", \"cancelled\"\nrequired: true",
          "type": "string"
        },
        "Status": {
          "description": "Status is a short text snippet for humans explaining the current state.",
          "type": "string"
        },
        "TaskErrorStacks": {
          "description": "This list of previous task lists and current tasks to handle errors.\nUpon completing the list, the previous task list will be executed.\n\nThis will be capped to a depth of 1.  Error failures can not be handled.",
          "items": {
            "$ref": "#/$defs/TaskStack"
          },
          "type": "array"
        },
        "Tasks": {
          "description": "The current tasks that are being processed.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Uuid": {
          "$ref": "#/$defs/UUID",
          "description": "Uuid is the key of this particular WorkOrder.\nrequired: true\nswagger:strfmt uuid"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        }
      },
      "type": "object"
    }
  },
  "$id": "https://gitlab.com/rackn/provision/v4/models/work-order",
  "$ref": "#/$defs/WorkOrder",
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}
