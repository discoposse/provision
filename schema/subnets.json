{
  "$defs": {
    "DhcpOption": {
      "additionalProperties": false,
      "description": "DhcpOption is a representation of a specific DHCP option.",
      "properties": {
        "Code": {
          "description": "Code is a DHCP Option Code.\n\nrequired: true",
          "type": "integer"
        },
        "Value": {
          "description": "Value is a text/template that will be expanded\nand then converted into the proper format\nfor the option code\n\nrequired: true",
          "type": "string"
        }
      },
      "type": "object"
    },
    "Meta": {
      "description": "Meta holds information about arbitrary things.",
      "patternProperties": {
        ".*": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "Subnet": {
      "additionalProperties": false,
      "description": "Subnet represents a DHCP Subnet.",
      "properties": {
        "ActiveEnd": {
          "description": "ActiveEnd is the last non-reserved IP address we will hand\nnon-reserved leases from.\n\nrequired: true\nswagger:strfmt ipv4",
          "format": "ipv4",
          "type": "string"
        },
        "ActiveLeaseTime": {
          "description": "ActiveLeaseTime is the default lease duration in seconds\nwe will hand out to leases that do not have a reservation.\n\nrequired: true",
          "type": "integer"
        },
        "ActiveStart": {
          "description": "ActiveStart is the first non-reserved IP address we will hand\nnon-reserved leases from.\n\nrequired: true\nswagger:strfmt ipv4",
          "format": "ipv4",
          "type": "string"
        },
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Description": {
          "description": "A description of this Subnet.  This should tell what it is for,\nany special considerations that should be taken into account when\nusing it, etc.",
          "type": "string"
        },
        "Documentation": {
          "description": "Documentation of this subnet.  This should tell what\nthe subnet is for, any special considerations that\nshould be taken into account when using it, etc. in rich structured text (rst).",
          "type": "string"
        },
        "Enabled": {
          "description": "Enabled indicates if the subnet should hand out leases or continue operating\nleases if already running.\n\nrequired: true",
          "type": "boolean"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "Name": {
          "description": "Name is the name of the subnet.\nSubnet names must be unique\n\nrequired: true",
          "type": "string"
        },
        "NextServer": {
          "description": "NextServer is the address of the next server in the DHCP/TFTP/PXE\nchain.  You should only set this if you want to transfer control\nto a different DHCP or TFTP server.\n\nrequired: true\nswagger:strfmt ipv4",
          "format": "ipv4",
          "type": "string"
        },
        "OnlyReservations": {
          "description": "OnlyReservations indicates that we will only allow leases for which\nthere is a preexisting reservation.\n\nrequired: true",
          "type": "boolean"
        },
        "Options": {
          "items": {
            "$ref": "#/$defs/DhcpOption"
          },
          "type": "array"
        },
        "Pickers": {
          "description": "Pickers is list of methods that will allocate IP addresses.\nEach string must refer to a valid address picking strategy.  The current ones are:\n\n\"none\", which will refuse to hand out an address and refuse\nto try any remaining strategies.\n\n\"hint\", which will try to reuse the address that the DHCP\npacket is requesting, if it has one.  If the request does\nnot have a requested address, \"hint\" will fall through to\nthe next strategy. Otherwise, it will refuse to try any\nremaining strategies whether or not it can satisfy the\nrequest.  This should force the client to fall back to\nDHCPDISCOVER with no requsted IP address. \"hint\" will reuse\nexpired leases and unexpired leases that match on the\nrequested address, strategy, and token.\n\n\"nextFree\", which will try to create a Lease with the next\nfree address in the subnet active range.  It will fall\nthrough to the next strategy if it cannot find a free IP.\n\"nextFree\" only considers addresses that do not have a\nlease, whether or not the lease is expired.\n\n\"mostExpired\" will try to recycle the most expired lease in the subnet's active range.\n\nAll of the address allocation strategies do not consider\nany addresses that are reserved, as lease creation will be\nhandled by the reservation instead.\n\nWe will consider adding more address allocation strategies in the future.\n\nrequired: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Proxy": {
          "description": "Proxy indicates if the subnet should act as a proxy DHCP server.\nIf true, the subnet will not manage ip addresses but will send\noffers to requests.  It is an error for Proxy and Unmanaged to be\ntrue.\n\nrequired: true",
          "type": "boolean"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "ReservedLeaseTime": {
          "description": "ReservedLeasTime is the default lease time we will hand out\nto leases created from a reservation in our subnet.\n\nrequired: true",
          "type": "integer"
        },
        "Strategy": {
          "description": "Strategy is the leasing strategy that will be used determine what to use from\nthe DHCP packet to handle lease management.\n\nrequired: true",
          "type": "string"
        },
        "Subnet": {
          "description": "Subnet is the network address in CIDR form that all leases\nacquired in its range will use for options, lease times, and NextServer settings\nby default\n\nrequired: true\npattern: ^([0-9]+\\.){3}[0-9]+/[0-9]+$",
          "type": "string"
        },
        "Unmanaged": {
          "description": "Unmanaged indicates that dr-provision will never send\nboot-related options to machines that get leases from this\nsubnet.  If false, dr-provision will send whatever boot-related\noptions it would normally send.  It is an error for Unmanaged and\nProxy to both be true.\n\nrequired: true",
          "type": "boolean"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        }
      },
      "type": "object"
    }
  },
  "$id": "https://gitlab.com/rackn/provision/v4/models/subnet",
  "$ref": "#/$defs/Subnet",
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}
