{
  "$defs": {
    "ArchInfo": {
      "additionalProperties": false,
      "description": "ArchInfo tracks information required to make a BootEnv work across different system architectures.",
      "properties": {
        "BootParams": {
          "description": "A template that will be expanded to create the full list of\nboot parameters for the environment.  If empty, this will fall back\nto the top-level BootParams field in the BootEnv\n\nrequired: true",
          "type": "string"
        },
        "Initrds": {
          "description": "Partial paths to the initrds that should be loaded for the boot\nenvironment. These should be paths that the initrds are located\nat in the OS ISO or install archive.  If empty, this will fall back\nto the top-level Initrds field in the BootEnv\n\nrequired: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "IsoFile": {
          "description": "IsoFile is the name of the ISO file (or other archive)\nthat contains all the necessary information to be able to\nboot into this BootEnv for a given arch.\nAt a minimum, it must contain a kernel and initrd that\ncan be booted over the network.",
          "type": "string"
        },
        "IsoUrl": {
          "description": "IsoUrl is the location that IsoFile can be downloaded from, if any.\nThis must be a full URL, including the filename.  dr-provision does\nnot use this field internally.  drpcli and the UX use this field to\nprovide a default source for downloading the IsoFile.\n\nswagger:strfmt url",
          "type": "string"
        },
        "Kernel": {
          "description": "The partial path to the kernel for the boot environment.  This\nshould be path that the kernel is located at in the OS ISO or\ninstall archive.  If empty, this will fall back to the top-level\nKernel field in the BootEnv\n\nrequired: true",
          "type": "string"
        },
        "Loader": {
          "description": "Loader is the bootloader that should be used for this boot\nenvironment.  If left unspecified and not overridden by a subnet\nor reservation option, the following boot loaders will be used:\n\n* lpxelinux.0 on 386-pcbios platforms that are not otherwise using ipxe.\n\n* ipxe.pxe on 386-pcbios platforms that already use ipxe.\n\n* ipxe.efi on amd64 EFI platforms.\n\n* ipxe-arm64.efi on arm64 EFI platforms.\n\nThis setting will be overridden by Subnet and Reservation\noptions, and it will also only be in effect when dr-provision is\nthe DHCP server of record.  It will also be overridden by the corresponding\nentry in the Loaders field of the BootEnv, if present and secure boot is enabled by\nthe license.",
          "type": "string"
        },
        "Sha256": {
          "description": "Sha256 should contain the SHA256 checksum for the IsoFile.\nIf it does, the IsoFile will be checked upon upload to make sure\nit has not been corrupted.",
          "type": "string"
        }
      },
      "type": "object"
    },
    "BootEnv": {
      "additionalProperties": false,
      "description": "BootEnv encapsulates the machine-agnostic information needed by the provisioner to set up a boot environment.",
      "properties": {
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "BootParams": {
          "description": "A template that will be expanded to create the full list of\nboot parameters for the environment.  This list will generally be passed as command line\narguments to the Kernel as it boots up.\n\nrequired: true",
          "type": "string"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Description": {
          "description": "Description is a one-line description of this boot environment.  This should tell what\nthe boot environment is for, any special considerations that\nshould be taken into account when using it, etc.",
          "type": "string"
        },
        "Documentation": {
          "description": "Documentation for this boot environment.  This should tell what\nthe boot environment is for, any special considerations that\nshould be taken into account when using it, etc. in rich structured text (rst).",
          "type": "string"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Initrds": {
          "description": "Partial paths to the initrds that should be loaded for the boot\nenvironment. These should be paths that the initrds are located\nat in the OS ISO or install archive.\n\nrequired: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Kernel": {
          "description": "The partial path to the kernel for the boot environment.  This\nshould be path that the kernel is located at in the OS ISO or\ninstall archive.  Kernel must be non-empty for a BootEnv to be\nconsidered net bootable.\n\nrequired: true",
          "type": "string"
        },
        "Loaders": {
          "description": "Loaders contains the boot loaders that should be used for various different network\nboot scenarios.  It consists of a map of machine type -> partial paths to the bootloaders.\nValid machine types are:\n\n- 386-pcbios for x86 devices using the legacy bios.\n\n- amd64-uefi for x86 devices operating in UEFI mode\n\n- arm64-uefi for arm64 devices operating in UEFI mode\n\nOther machine types will be added as dr-provision gains support for them.\n\nIf this map does not contain an entry for the machine type, the DHCP server will fall back to\nthe following entries in this order:\n\n- The Loader specified in the ArchInfo struct from this BootEnv, if it exists.\n\n- The value specified in the bootloaders param for the machine type specified on the machine, if it exists.\n\n- The value specified in the bootloaders param in the global profile, if it exists.\n\n- The value specified in the default value for the bootloaders param.\n\n- One of the following vaiues:\n\n  - lpxelinux.0 for 386-pcbios\n\n  - ipxe.efi for amd64-uefi\n\n  - ipxe-arm64.efi for arm64-uefi\n\nrequired: true",
          "patternProperties": {
            ".*": {
              "type": "string"
            }
          },
          "type": "object"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "Name": {
          "description": "Name is the name of the boot environment.  Boot environments that install\nan operating system must end in '-install'.  All boot environment names must be unique.\n\nrequired: true",
          "type": "string"
        },
        "OS": {
          "$ref": "#/$defs/OsInfo",
          "description": "OS is the operating system specific information for the boot environment."
        },
        "OnlyUnknown": {
          "description": "OnlyUnknown indicates whether this bootenv can be used without a\nmachine.  Only bootenvs with this flag set to `true` be used for\nthe unknownBootEnv preference.  If this flag is set to True, then the\nTemplates provided byt this boot environment must take care to be able\nto chainload into the appropriate boot environments for other machines\nif the bootloader that machine is using does not support it natively.\nThe built-in ignore boot environment and the discovery boot environment\nprovided by the community content bundle should be used as references for\nsatisfying that requirement.\n\nrequired: true",
          "type": "boolean"
        },
        "OptionalParams": {
          "description": "The list of extra optional parameters for this\nboot environment. They can be present as Machine.Params when\nthe bootenv is applied to the machine.  These are more\nother consumers of the bootenv to know what parameters\ncould additionally be applied to the bootenv by the\nrenderer based upon the Machine.Params",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "RequiredParams": {
          "description": "The list of extra required parameters for this\nboot environment. They should be present as Machine.Params when\nthe bootenv is applied to the machine.\n\nrequired: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Templates": {
          "description": "Templates contains a list of templates that should be expanded into files for the\nboot environment.  These expanded templates will be available via TFTP and static HTTP\nfrom dr-provision.  You should take care that the final paths for the temmplates do not\noverlap with ones provided by other boot environments.\n\nrequired: true",
          "items": {
            "$ref": "#/$defs/TemplateInfo"
          },
          "type": "array"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        }
      },
      "type": "object"
    },
    "Meta": {
      "description": "Meta holds information about arbitrary things.",
      "patternProperties": {
        ".*": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "OsInfo": {
      "additionalProperties": false,
      "description": "OsInfo holds information about the operating system this BootEnv maps to.",
      "properties": {
        "Codename": {
          "description": "The codename of the OS, if any.",
          "type": "string"
        },
        "Family": {
          "description": "The family of operating system (linux distro lineage, etc)",
          "type": "string"
        },
        "IsoFile": {
          "description": "The name of the ISO that the OS should install from.  If\nnon-empty, this is assumed to be for the amd64 hardware\narchitecture.",
          "type": "string"
        },
        "IsoSha256": {
          "description": "The SHA256 of the ISO file.  Used to check for corrupt downloads.\nIf non-empty, this is assumed to be for the amd64 hardware\narchitecture.",
          "type": "string"
        },
        "IsoUrl": {
          "description": "The URL that the ISO can be downloaded from, if any.  If\nnon-empty, this is assumed to be for the amd64 hardware\narchitecture.\n\nswagger:strfmt uri",
          "type": "string"
        },
        "Name": {
          "description": "The name of the OS this BootEnv has.  It should be formatted as\nfamily-version.\n\nrequired: true",
          "type": "string"
        },
        "SupportedArchitectures": {
          "description": "SupportedArchitectures maps from hardware architecture (named\naccording to the distro architecture naming scheme) to the\narchitecture-specific parameters for this OS.  If\nSupportedArchitectures is left empty, then the system assumes\nthat the BootEnv only supports amd64 platforms.",
          "patternProperties": {
            ".*": {
              "$ref": "#/$defs/ArchInfo"
            }
          },
          "type": "object"
        },
        "Version": {
          "description": "The version of the OS, if any.",
          "type": "string"
        }
      },
      "type": "object"
    },
    "TemplateInfo": {
      "additionalProperties": false,
      "description": "TemplateInfo holds information on the templates in the boot environment that will be expanded into files.",
      "properties": {
        "Contents": {
          "description": "Contents that should be used when this template needs\nto be expanded.  Either this or ID should be set.\n\nrequired: false",
          "type": "string"
        },
        "ID": {
          "description": "ID of the template that should be expanded.  Either\nthis or Contents should be set\n\nrequired: false",
          "type": "string"
        },
        "Link": {
          "description": "Link optionally references another file to put at\nthe path location.",
          "type": "string"
        },
        "Meta": {
          "description": "Meta for the TemplateInfo.  This can be used by the job running\nsystem and the bootenvs to handle OS, arch, and firmware differences.\n\nrequired: false",
          "patternProperties": {
            ".*": {
              "type": "string"
            }
          },
          "type": "object"
        },
        "Name": {
          "description": "Name of the template\n\nrequired: true",
          "type": "string"
        },
        "Path": {
          "description": "A text/template that specifies how to create\nthe final path the template should be\nwritten to.\n\nrequired: true",
          "type": "string"
        }
      },
      "type": "object"
    }
  },
  "$id": "https://gitlab.com/rackn/provision/v4/models/boot-env",
  "$ref": "#/$defs/BootEnv",
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}
